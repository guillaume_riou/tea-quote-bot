import irc, strutils, std/re, os, sequtils, random, threadpool
{.experimental: "parallel".}

# patterns of messages to completely ignore
let ignore_patterns = @[
  re"---\s.*",
  re"\d{2}:\d{2}\s\-\!\-\s.+",
  re".*THIS\sCHANNEL\sHAS\sMOVED\sTO\sIRC\.FREENODE\.NET.*",
  re".*vlepy.github.io.*"
]
# pattern that matcher hour+username
let legit_message_header = re"\d{2}:\d{2}\s<\s[^>]+>\s"

var logs = toSeq(walkDir("logs", relative=true)).mapIt($(readFile("logs/"&it.path))).join().split("\n")

proc apply_all_ignore_regex(line: string): string =
  # I need to ignore the fact that the compiler tells me this proc is not gcsafe.
  # the std/re module does not support declaring a compiled regex as const so the compiler
  # panics and thinks that legit_message_header can possibly be collected while the thread worker
  # uses it.
  {.cast(gcsafe).}:
    var result_line: string
    for regex in ignore_patterns:
      result_line = replace(line, regex, "")
      if result_line == "":
        return result_line
    return replace(result_line, legit_message_header, "")

# non parallel version.
var quotes = logs.map(apply_all_ignore_regex).filter(proc(log: string):bool = log.len > 0)
# the parallel version is actually less performant (and slightly broken) but its funny so I leave it there.
# parallel:
  # var filtered_logs = newSeq[string](logs.high+1)
  # for i in 0..logs.high:
    # filtered_logs[i] = ^ spawn apply_all_ignore_regex(logs[i])
# var quotes = filtered_logs.filter(proc(log: string):bool = log.len > 0)

randomize()
var chan = @["##tryoutnimirc"]
# var chan = @["#diroum"]

var client = newIrc("irc.libera.chat", nick="tea-quote-bot",
                    joinChans = chan)


let bang_word_regex = re"![^\s]"
client.connect()
while true:
  var event: IrcEvent
  if client.poll(event):
    case event.typ
    of EvConnected:
      discard
    of EvDisconnected, EvTimeout:
      client.reconnect()
      break
    of EvMsg:
      if event.cmd == MPrivMsg and event.text == "!quote":
        client.privmsg(event.origin, quotes.sample())
      elif event.cmd == MPrivMsg and event.text.match(bang_word_regex):
        let quotes = quotes.filter(proc(quote: string): bool = event.text[1 .. ^1] in quote)
        if quotes.len > 0:
          client.privmsg(event.origin, quotes.sample())
