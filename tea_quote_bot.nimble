[Package]
name          = "tea_quote_bot"
version       = "0.1.0"
author        = "guillaume riou"
description   = "bot for ##tea quote"
license       = "MIT"

srcDir = "src"

bin = "tea_quote_bot"

[Deps]
Requires: "nimrod >= 0.14, irc 0.4.0, regex 0.20.1"